package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/signald/signald-go/cmd/signaldctl/common"
	"gitlab.com/signald/signald-go/signald"
)

var rawCmd = &cobra.Command{
	Use:   "raw version type [json]",
	Short: "make a request to signald",
	Long: `make a raw request to signald, returning the result. If request data is not provided via command line argument labeled \"json\", it must be provided via stdin.
	
Example usage:

	signaldctl raw v1 get_profile '{"account": "+12024561414", "address": {"number": "+12024561111"}}'`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 3 {
			return cmd.Help()
		}

		var reqdata map[string]interface{}
		if err := json.Unmarshal([]byte(args[2]), &reqdata); err != nil {
			return err
		}

		requestID := signald.GenerateID()
		reqdata["id"] = requestID
		reqdata["type"] = args[1]
		reqdata["version"] = args[0]

		go common.Signald.Listen(nil)

		err := common.Signald.RawRequest(reqdata)
		if err != nil {
			log.Println("error submitting request to signald")
			return err
		}

		responseChannel := common.Signald.GetResponseListener(requestID)
		defer common.Signald.CloseResponseListener(requestID)

		rawResponse := <-responseChannel
		if rawResponse.Error != nil {
			return fmt.Errorf("signald error: %s", string(rawResponse.Error))
		}

		_, err = os.Stdout.Write(rawResponse.Data)
		return err
	},
}

func init() {
	RootCmd.AddCommand(rawCmd)
}
